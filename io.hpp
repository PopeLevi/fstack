#ifndef IO_H
#define IO_H

#include <iostream>
#include <fstream>

#include <cstdio>

#include <sys/stat.h>

namespace IO
{
    inline bool Exists(std::string file)
    {
	std::ifstream input(file);
	return input;
    }

    inline bool Copy(std::string src, std::string dest)
    {
	std::ifstream input(src);
	std::ofstream output(dest);

	bool ret = input && output;

	if (ret)
	    output << input.rdbuf();

	input.close();
	output.close();

	return ret;
    }

    inline bool Move(std::string src, std::string dest)
    {
	return IO::Copy(src, dest) && !std::remove(src.c_str());
    }

    inline bool Delete(std::string file)
    {
	return !std::remove(file.c_str());
    }

    inline std::string GetFilename(std::string path)
    {
	int index = -1;
	
	for (int i = path.length() - 1; i; i--)
	{
	    if (path[i] == SEPERATOR)
	    {
		index = i + 1;
		break;
	    }
	}

	return (index != -1 ? path.substr(index, path.length()) : path);
    }
}

#endif
