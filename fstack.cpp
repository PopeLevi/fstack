#include <fstream>
#include <string>
#include <algorithm>

#include <unistd.h>

#include "fstack.hpp"
#include "io.hpp"

#include "trex/TRexpp.h"

fstack::fstack(int argc, char* argv[])
{
    std::ifstream stackfile(STACKFILE);
    std::ifstream aliasfile(ALIASFILE);

    CreateDir(HOME_DIR); // Will fail silently if the directory already exists

    functionMap["add"] = [](fstack& fs, std::vector<std::string> v)    { fs.f_add(v); };
    functionMap["remove"] = [](fstack& fs, std::vector<std::string> v) { fs.f_remove(v); };
    functionMap["copy"] = [](fstack& fs, std::vector<std::string> v)   { fs.f_copy(v); };
    functionMap["move"] = [](fstack& fs, std::vector<std::string> v)   { fs.f_move(v); };
    functionMap["delete"] = [](fstack& fs, std::vector<std::string> v) { fs.f_delete(v); };
    functionMap["at"] = [](fstack& fs, std::vector<std::string> v)     { fs.f_at(v); };
    functionMap["alias"] = [](fstack& fs, std::vector<std::string> v)  { fs.f_alias(v); };

    // If the stack file exists, read it into memory, otherwise create an empty file
    if (stackfile)
    {
	while (!stackfile.eof())
	{
	    std::string line;
	    std::getline(stackfile, line);

	    if (!line.empty())
	    {
		if (IO::Exists(line))
		    files.push_back(line);
		else
		    std::cout << "File `" << line << "` does not exist" << std::endl;
	    }
	}

	stackfile.close();

    } else {
	std::cout << "Initialising stack\n" << std::endl;
	
	std::ofstream file(STACKFILE);
	file.close();
    }

    if (aliasfile)
    {
	while (!aliasfile.eof())
	{
	    std::string line;
	    std::getline(aliasfile, line);

	    TRexpp expr;
	    expr.Compile("^.+=.+$");

	    if (expr.Match(line.c_str()))
	    {
		std::string key = line.substr(0, line.find('='));
		std::string value = line.substr(line.find('=') + 1, line.length());

		if (std::find(files.begin(), files.end(), value) != files.end())
		    aliases[key] = value;
	    }
	}
    }

    this->write();

    switch (argc)
    {
	case 1:
	    std::cout << "Incorrect number of args. Try `fstack --help`" << std::endl;
	    break;
	    
	case 2:
	    if ((std::string)argv[1] == "--help") this->printHelp();
	    else if ((std::string)argv[1] == "list") this->f_list();
	    else if ((std::string)argv[1] == "clear") this->f_clear();
	    else if ((std::string)argv[1] == "total") this->f_total();
	    else std::cout << "Incorrect number of args. Try `fstack --help`" << std::endl;
	    
	    break;
		
	default:
	    if (functionMap.find(argv[1]) == functionMap.end())
		std::cout << "Command not found: `" << argv[1] << "`. Try `fstack --help`" << std::endl;
	    else
		functionMap[argv[1]](*this, this->makeArgs(argc, argv)); // Dispatch commands
		
	    break;
    }
}


// Adds files to the stack if they do not already exist on the stack
void fstack::f_add(std::vector<std::string> args)
{
    std::cout << std::endl;
    
    for (std::string file : args)
    {
	std::string fullName;
	TRexpp rootExpr;
	rootExpr.Compile(ROOT_EXPR);

	// If the filename starts with the root directory, add it as-is, otherwise get the working directory and append the filename to that
	fullName = AbsPath(rootExpr.Match(file.c_str()) ? file :
			   []() -> std::string { char c[256]; getcwd(c, sizeof(c)); return (std::string)c; }() +
			   SEPERATOR + file);

	// If the file is already part of the stack, inform the user and return
	if (std::find(files.begin(), files.end(), fullName) != files.end())
	{
	    std::cout << "\tFile `" << file << "` is already part of the stack" << std::endl;
	} else {

	    if (IO::Exists(fullName))
	    {
		std::cout << "\tFile `" << fullName << "` added" << std::endl;
		files.push_back(fullName);
		this->write();
	    } else {
		std::cout << "\tFile `" << fullName << "` does not exist" << std::endl;
	    }
	}
    }
}


// Removes files from the stack
void fstack::f_remove(std::vector<std::string> args)
{
    std::cout << std::endl;
    
    for (std::string s : args)
    {
	std::vector<std::string> range;
	std::string file = this->parseAccessor(s, range);
	
	if (!file.empty())
	{
	    files.erase(std::find(files.begin(), files.end(), file));
	    std::cout << "\tRemoved `" << file << "` from the stack" << std::endl;
	    
	} else if (!range.empty()) {
	    
	    for (std::string item : range)
	    {
		files.erase(std::find(files.begin(), files.end(), item));
		std::cout << "\tRemoved `" << item << "` from the stack" << std::endl;
	    }
	    
	} else {
	    std::cout << "Invalid parameter `" << s << "`\nTry `fstack --help`" << std::endl;
	}

	this->write();
    }
}


// Copies files from the stack to a specified destination
void fstack::f_copy(std::vector<std::string> args)
{
    std::cout << std::endl;
    if (args.size() != 2)
    {
	std::cout << "Incorrect number of arguments.\nTry\n\t`fstack copy I:[INDEX] <DEST>`\nor\n\t`fstack copy L:[INDEX1]:[INDEX2]`" << std::endl;
	return;
    }

    std::vector<std::string> range;
    std::string file = this->parseAccessor(args[0], range);

    if (!file.empty())
    {
	if (IO::Exists(args[1]))
	{
	    if (this->ynPrompt("\tFile `" + args[1] + "` exists.\nOverwrite? ")) IO::Delete(args[1]);
	    else return;
	}

	if (IO::Copy(file, args[1]))
	    std::cout << "\tCopied `" << file << "` to `" << args[1] << "`" << std::endl;
	else
	    std::cout << "\tUnable to copy `" << file << "` to `" << args[1] << "`" << std::endl;
	    
    } else if (range.size() > 0) {

	if (IO::Exists(args[1]))
	{
	    std::cout << "\t`" << args[1] << "` is a file" << std::endl;
	    return;
	}

	for (std::string s : range)
	{
	    if (!IO::Exists(s))
	    {
		std::cout << "\tFile `" << s << "` does not exist" << std::endl;
		continue;
	    }

	    if (IO::Copy(s, args[1] + SEPERATOR + IO::GetFilename(s)))
		std::cout << "\tCopied `" << s << "` to `" << args[1] << SEPERATOR << IO::GetFilename(s) << "`"  << std::endl;
	    else
		std::cout << "\tUnable to copy `" << s << "` to `" << args[1] << SEPERATOR << IO::GetFilename(s) << "`" << std::endl;
	}

    } else {
	std::cout << "\tUnrecognised accessor `" << args[0] << '`' << std::endl;
    }
}


// Moves files from the stack to the specified destination
void fstack::f_move(std::vector<std::string> args)
{
    if (args.size() != 2)
    {
	std::cout << "Incorrect number of arguments.\nTry\n\t`fstack move I:[INDEX] <DEST>`\nor\n\t`fstack move L:[INDEX1]:[INDEX2]`" << std::endl;
	return;
    }

    std::vector<std::string> range;
    std::string file = this->parseAccessor(args[0], range);

    if (!file.empty())
    {
	if (IO::Exists(args[1]))
	{
	    if (this->ynPrompt("\tFile `" + args[1] + "` exists.\nOverwrite? "))
		IO::Delete(args[1]);
	    else return;
	}

	if (IO::Move(file, args[1]))
	{
	    std::cout << "\tMoved `" << file << "` to `" << args[1] << "`" << std::endl;

	    std::string newFile;
	    TRexpp expr;
	    expr.Compile(ROOT_EXPR);

	    // If args[1] does not contain an absolute path, make newFile contain the absolute path of args[1]
	    if (!expr.Match(args[1].c_str()))
		newFile = AbsPath([]() -> std::string { char c[256]; getcwd(c, sizeof(c)); return (std::string)c; }() + SEPERATOR + args[1]);
	    else
		newFile = args[1];
	    
	    // Remove the original file from the stack, and add the destination file
	    files.erase(std::find(files.begin(), files.end(), file));
	    files.push_back(newFile);
	    this->write();

	} else {
	    std::cout << "\tUnable to move `" << file << "` to `" << args[1] << "`" << std::endl;
	}
	
    } else if  (!range.empty()) {

	if (IO::Exists(args[1]))
	{
	    std::cout << "\t`" << args[1] << "` is a file." << std::endl;
	    return;
	}

	for (std::string s : range)
	{
	    if (!IO::Exists(s))
	    {
		std::cout << "\tFile `" << s << "` does not exist" << std::endl;
		continue;
	    }

	    std::string newName = args[1] + SEPERATOR + IO::GetFilename(s);

	    // TODO: Make sure we can move all selected files
	    if (IO::Move(s, newName))
	    {
		std::cout << "\tMoved `" << s << "` to `" << newName << "`" << std::endl;

		TRexpp expr;
		expr.Compile(ROOT_EXPR);

		if (!expr.Match(newName.c_str()))
		    newName = AbsPath([]() -> std::string { char c[256]; getcwd(c, sizeof(c)); return (std::string)c; }() + SEPERATOR + IO::GetFilename(newName));

		files.erase(std::find(files.begin(), files.end(), s));
		files.push_back(newName);
		this->write();
		
	    } else {
		std::cout << "\tUnable to move `" << s << "` to `" << args[1] << SEPERATOR << IO::GetFilename(s) << "`" << std::endl;
	    }
	}
	
    } else {
	std::cout << "\tUnrecognised accessor `" << args[0] << "`" << std::endl;
    }
}


// Deletes the specified files
void fstack::f_delete(std::vector<std::string> args)
{
    for (std::string s : args)
    {
	std::vector<std::string> range;
	std::string file = this->parseAccessor(s, range);

	if (!files.empty())
	{
	    if (IO::Exists(file)) // Delete the file and remove it from the stack if the file exists
	    {
		IO::Delete(file);
		files.erase(std::find(files.begin(), files.end(), file));
		this->write();
		
	    } else {
		std::cout << "\tFile `" << file << "` does not exist" << std::endl;
	    }

	} else if (!range.empty()) {

	    for (std::string item : range)
	    {
		if (IO::Exists(item))
		{
		    IO::Delete(item);
		    files.erase(std::find(files.begin(), files.end(), file));
		    this->write();
		    
		} else {
		    std::cout << "\tFile `" << file << "` does not exist" << std::endl;
		}
	    }
	    
	} else {
	    std::cout << "\tUnrecognised accessor `" << s << "`" << std::endl;
	}
    }
}


// Prints the names of the files at the specified indices
void fstack::f_at(std::vector<std::string> args)
{
    TRexpp expr;
    expr.Compile("^[0-9]+$");

    std::cout << std::endl;
    
    for (std::string s : args)
    {
	if (!expr.Match(s.c_str()))
	{
	    std::cout << "\t`" << s << "` is not a number" << std::endl;
	    continue;
	}
	
	int index = atoi(s.c_str());

	if (index >= files.size())
	    std::cout << "\tInvalid index `" << index << "`" << std::endl;
	else
	    std::cout << "\t" << files[index] << std::endl;
    }
}


// Bind identifiers to files on the stack
void fstack::f_alias(std::vector<std::string> args)
{
    TRexpp expr;
    
    for (int i = 0; i < args.size(); i++)
    {
	if (args[i] == "add")
	{
	    if (++i >= args.size())
	    {
		std::cout << "Expression expected" << std::endl;
		break;
	    }
	    
	    std::string s = args[i];

	    if (expr.Compile("^.+=[iI]\\:[0-9]+$") && expr.Match(s.c_str()))
	    {
		std::string rawIndex = s.substr(s.find('=') + 1, s.length());
		int index = atoi(s.substr(s.find(':') + 1, s.length()).c_str());
		
		if (index < files.size())
		    aliases[s.substr(0, s.find('='))] = files[index];
		else
		    std::cout << "Index must be less than the total number of files on the stack" << std::endl;
		
	    } else if (expr.Compile("^.+=[fF]\\:.+$") && expr.Match(s.c_str())) {

		std::string file = s.substr(s.find('=') + 1, s.length());
		file = file.substr(file.find(':') + 1, file.length());

		if (std::find(files.begin(), files.end(), file) != files.end())
		    aliases[s.substr(0, s.find('='))] = file;
		else
		    std::cout << "\t`" << file << "` is not part of the stack" << std::endl;
		    
	    } else {
		std::cout << "Invalid expression `" << s  << "`" << std::endl;
	    }
	    
	} else if (args[i] == "remove") {

	    if (++i >= args.size())
	    {
		std::cout << "Expression expected" << std::endl;
		break;
	    }

	    std::string s = args[i];
	    if (aliases.find(s) != aliases.end())
		aliases.erase(aliases.find(s));
	    else
		std::cout << "\n\t`" << s << "` has not been declared as an alias" << std::endl;
	    
	} else if (args[i] == "get") {

	    if (aliases.find(args[++i]) != aliases.end())
		std::cout << "\n\t" << args[i] << " = " << aliases[args[i]] << std::endl;
	    else
		std::cout << "\n\t`" << args[i] << "` has not been declared as an alias" << std::endl;

	} else if (args[i] == "list") {

	    std::cout << std::endl;
	    
	    for (auto kv : aliases)
		std::cout << "\t" << kv.first << " = " << kv.second << std::endl;
	    
	} else if (args[i] == "clear") {

	    if (this->ynPrompt("Clear all aliases?"))
	    {
		aliases.erase(aliases.begin(), aliases.end());
		this->write();

		std::cout << "Aliases cleared." << std::endl;
	    }
	    
	} else {
	    std::cout << "\n`" << args[i] << "` is not a valid command. Try `fstack --help`" << std::endl;
	}
    }

    this->write();
}


// List the files on the stack
void fstack::f_list()
{
    std::cout << std::endl;
    
    for (std::string s : files)
	std::cout << "\t" << s << std::endl;
}


// Remove all items on the stack
void fstack::f_clear()
{
    if (this->ynPrompt("Clear the stack?"))
    {
	// Empty the file
	files.erase(files.begin(), files.end());
	this->write();

	std::cout << "Stack cleared." << std::endl;
    }
}


// Print the total number of files on the stack
void fstack::f_total()
{
    std::cout << "\t" << files.size() << " total files" << std::endl;
}


// Turns the argument vector into an std::vector for easier parsing
std::vector<std::string> fstack::makeArgs(int argc, char* argv[])
{
    std::vector<std::string> ret;

    for (int i = 2; i < argc; i++)
	ret.push_back(argv[i]);
    
    return ret;
}


// Parses accessors (I:INDEX, F:FILENAME, L:INDEX1:INDEX2, A:ALIAS) to file names
std::string fstack::parseAccessor(std::string pattern)
{
    std::vector<std::string> dummy;
    return this->parseAccessor(pattern, dummy);
}

std::string fstack::parseAccessor(std::string pattern, std::vector<std::string>& list)
{
    std::string ret;
    TRexpp expr;

    expr.Compile(INDEX_EXPR);
    if (expr.Match(pattern.c_str()))
    {
	int index = atoi(pattern.substr(pattern.find(':') + 1, pattern.length()).c_str());

	if (index < files.size())
	    ret = files[index];
	else
	    std::cout << "Index must be smaller than the number of files on the stack" << std::endl;
	    
    } else if (expr.Compile(FILE_EXPR) && expr.Match(pattern.c_str())) {

	std::string fname = pattern.substr(pattern.find(':') + 1, pattern.length());
	
	if (std::find(files.begin(), files.end(), fname) != files.end())
	    ret = fname;
	else
	    std::cout << "`" << fname << "` is not part of the stack" << std::endl;

    } else if (expr.Compile(LIST_EXPR) && expr.Match(pattern.c_str())) {

	std::string indices = pattern.substr(pattern.find(':') + 1, pattern.length());
	int ind1 = atoi(indices.substr(0, indices.find(':')).c_str());
	int ind2 = atoi(indices.substr(indices.find(":") + 1, indices.length()).c_str());

	if (ind1 < ind2 && ind2 + 1 < files.size())
	{
	    if (!list.empty())
		list.erase(list.begin(), list.end());

	    list.resize(ind2 - ind1);
	    std::copy(files.begin() + ind1, files.begin() + ind2 + 1, list.begin());
	} else {
	    std::cout << "\tInvalid indices" << std::endl;
	}

    } else if (expr.Compile(ALIAS_EXPR) && expr.Match(pattern.c_str())) {

	std::string identifier = pattern.substr(pattern.find(':') + 1, pattern.length());
	ret = aliases.find(identifier) != aliases.end() ? aliases[identifier] : std::string();
    }

    return ret;
}


// Shows a simple '(y/n)' prompt with a specified message
bool fstack::ynPrompt(std::string message)
{
    char c = 0;

    while (c != 'y' && c != 'n')
    {
	std::cout << message << " (y/n)";
	std::cin >> c;
	std::cout << std::endl;

	if (c == 'Y') c = 'y';
	if (c == 'N') c = 'n';
    }

    return c == 'y';
}


// Prints the help message
void fstack::printHelp()
{
    // TODO: Finish help message
    std::cout << "fstack" << std::endl;
    std::cout << "Perfom basic file management on files in inconviently placed directories" << std::endl;
    std::cout << "through a stack-like interface" << std::endl;
    
    std::cout << "\nCommands" << std::endl;
    
    std::cout << "\tAdd [ fstack add <file1> <file2> ... ]" << std::endl;
    std::cout << "\t\tPushes one or more files onto the stack." << std::endl;
    std::cout << "\t\tThis allows fstack to perfom actions on the file(s)\n" << std::endl;

    std::cout << "\tRemove [ fstack remove <I:INDEX | F:FILENAME | L:LIST | A:ALIAS> ]" << std::endl;
}


// Writes the stack and aliases to files
void fstack::write()
{
    std::ofstream stackfile(STACKFILE);

    for (std::string s : files)
	stackfile << s << std::endl;

    stackfile.close();

    std::ofstream aliasfile(ALIASFILE);

    for (auto kv : aliases)
	aliasfile << kv.first << "=" << kv.second << std::endl;

    aliasfile.close();
}

