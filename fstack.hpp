#ifndef FSTACK_H
#define FSTACK_H

#include <iostream>
#include <vector>
#include <map>

#include "platform.hpp"

class fstack; // Messy fix so the typedef below knows what an fstack is

typedef void (*fstack_function)(fstack&, std::vector<std::string>);

class fstack
{
    public:
	fstack(int argc, char* argv[]);

    private:
	void f_add(std::vector<std::string> args);
	void f_remove(std::vector<std::string> args);
	void f_copy(std::vector<std::string> args);
	void f_move(std::vector<std::string> args);
	void f_delete(std::vector<std::string> args);
	void f_at(std::vector<std::string> args);
	void f_alias(std::vector<std::string> args);
	void f_list();
	void f_clear();
	void f_total();

	
	std::vector<std::string> makeArgs(int argc, char* argv[]);
	std::string parseAccessor(std::string pattern);
	std::string parseAccessor(std::string pattern, std::vector<std::string>& list);
	bool ynPrompt(std::string message);
	void printHelp();
	void write();

	std::vector<std::string> files;
	std::map<std::string, std::string> aliases;
	std::map<std::string, fstack_function> functionMap;

	const char* INDEX_EXPR = "^[iI]\\:[0-9]+$";
	const char* FILE_EXPR  = "^[fF]\\:.+$";
	const char* LIST_EXPR  = "^[lL]\\:[0-9]+:[0-9]+";
	const char* ALIAS_EXPR = "^[aA]\\:.+$";
};


#endif
