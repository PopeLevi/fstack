#ifndef PLATFORM_H
#define PLATFORM_H

#ifdef _WIN32

    #include <windows.h>
    #include <direct.h>

    #define HOME_DIR ([]() -> std::string \
    { \
        char c[512]; \
	GetEnvironmentVariable("APPDATA", c, sizeof(c)); \
	return (std::string)c + "\\.fstack"; \
    }())

    #define STACKFILE HOME_DIR + "\\stack"
    #define ALIASFILE HOME_DIR + "\\alias"

    #define SEPERATOR '\\'
    #define ROOT_EXPR "^([A-Z]\\:)?[\\\\/].*"

    #define CreateDir(X) _mkdir(X.c_str())

    #define AbsPath(X) ([](std::string s) -> std::string \
    { \
	char buffer[512] = { NULL }; \
    	GetFullPathName(s.c_str(), 512, buffer, NULL); \
	return (std::string)buffer; \
    }(X))

#elif defined __linux || defined __unix || defined __APPLE__

    #include <sys/types.h>
    #include <sys/stat.h>

    #define HOME_DIR  (std::string)getenv("HOME") + (std::string)"/.fstack"
    #define STACKFILE (std::string)HOME_DIR + "/stack"
    #define ALIASFILE (std::string)HOME_DIR + "/alias"

    #define SEPERATOR '/'
    #define ROOT_EXPR "^/.*"

    #define CreateDir(X) [](std::string dir) -> void { mkdir(dir.c_str(), 0777); }(X)

    #define AbsPath(X) ([](std::string s) -> std::string \
    { \
        char buffer[512]; \
	realpath(s.c_str(), buffer); \
	return (std::string)buffer; \
    }(X))

#else

    #error Unsupported OS

#endif

#endif
