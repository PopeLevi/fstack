CXX=clang++
C=gcc
CXXFLAGS=--std=c++11
OBJ_DIR=./obj
BIN_DIR=./bin
EXEC_NAME=fstack

all: clean main.o fstack.o trex.o
	$(CXX) $(OBJ_DIR)/main.o $(OBJ_DIR)/fstack.o $(OBJ_DIR)/trex.o -o $(BIN_DIR)/$(EXEC_NAME)

clean:
	if [ ! -d $(OBJ_DIR) ]; then mkdir $(OBJ_DIR); fi
	if [ ! -d $(BIN_DIR) ]; then mkdir $(BIN_DIR); fi

	cat /dev/null > $(OBJ_DIR)/dummy.o

	rm -r -f $(OBJ_DIR)/*.o
	if [ -f $(BIN_DIR)/$(EXEC_NAME) ]; then rm -f $(BIN_DIR)/$(EXEC_NAME); fi

main.o: main.cpp
	$(CXX) -g -c main.cpp -o $(OBJ_DIR)/main.o $(CXXFLAGS)

fstack.o: fstack.cpp
	$(CXX) -g -c fstack.cpp -o $(OBJ_DIR)/fstack.o $(CXXFLAGS)

trex.o: ./trex/trex.c
	$(C) -g -c ./trex/trex.c -o $(OBJ_DIR)/trex.o
